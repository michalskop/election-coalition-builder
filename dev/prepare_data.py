"""Prepare data from calculated values."""

import json

path = "/home/michal/project/election-coalition-builder/"

# fname = "current_seats.json"

original = {
    'SPOLU': 71,
    'ANO': 72,
    'Piráti+STAN': 37,
    'SPD': 20
}

original = {
    'ANO': 72,
    'ODS': 34,
    'STAN': 33,
    'KDU-ČSL': 23,
    'SPD': 20,
    'TOP 09': 14,
    'Piráti': 4,
}

parties = []
candidates = []

# Seznam Zpravy colors
def szc(c):
    if c == 'ODS':
        return '#004494'
    if c == 'ANO':
        return "#33CCCC"
    if c == 'Piráti':
        return "#222222"
    if c == 'SPD':
        return '#C05F2F',
    if c == 'KDU-ČSL':
        return '#e6ac21'
    if c == 'TOP 09':
        return '#723769'
    if c == 'STAN':
        return '#5d8c00'

i = 1
j = 1
for r in original:
    if r == 'Piráti+STAN':
        party_name = 'PirSTAN' #'PirSTAN'
    else:
        party_name = r

    party = {
        'name': party_name,
        'rank': i,
        'color': szc(r), # row['color'],
        'core': int(original[r]),
        'selected': (i == 1)
    }
    parties.append(party)
    
    p = 0
    for k in range(0, original[r]):
        candidate = {
            'party': party_name,
            'color': szc(r), # row['color'],
            'opacity': 1 - p * 0.6,
            'total_rank': j,
            'selected': (i == 1),
            # 'level': p,
            # 'position': {'x': 0, 'y': 0},
            'party_rank': i,
            # 'local_rank': m + 1
        }
        candidates.append(candidate)
        j += 1

    i += 1

len(candidates)
# candidates[0:5]

with open(path + "src/assets/parties.json", "w") as fout:
    json.dump(parties, fout)

candidates = sorted(candidates, key=lambda x: x['opacity'], reverse=True)

with open(path + "src/assets/candidates.json", "w") as fout:
    json.dump(candidates, fout)
